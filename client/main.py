import cv2
import mediapipe as mp
import requests
import datetime
import time
import json
import base64
import asyncio

mp_face_detection = mp.solutions.face_detection
mp_drawing = mp.solutions.drawing_utils

server="http://192.168.8.40:81/"
deviceid=-100
IMEI="XXXXXXXXXX"

#register in server
url=server+"register.php?id={}&IMEI={}".format(deviceid,IMEI)
requests.get(url=url)


# For webcam input:
cap = cv2.VideoCapture(0)
fourcc = cv2.VideoWriter_fourcc(*'XVID')



while True:

  while True:
    datetime_str = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    report_obj={ 'time' : datetime_str, 'status' : 'waiting', 'device' : 'raspberry pi', 'power' : 100, 'locked' : 1 }
    url=server+"report.php?id={}&IMEI={}".format(deviceid,IMEI)
    r=requests.post(url=url,data=json.dumps(report_obj))
    print(url)
    r.encoding = 'utf-8'
    
    print(r.status_code)
    print(r.text)
    
    receive=json.loads(r.text)
    if receive['command']=='record':
      break
    time.sleep(1)
    
  
  if receive['command']=='shutdown':
    break
  
  time0=time.time()
  lastreporttime=0
  starttime=datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
  
  
  
  out = cv2.VideoWriter('output.avi', fourcc, 20.0, (640,480))
  with mp_face_detection.FaceDetection(model_selection=0, min_detection_confidence=0.4) as face_detection:
    while cap.isOpened():
      success, image = cap.read()
      if not success:
        print("Ignoring empty camera frame.")
        # If loading a video, use 'break' instead of 'continue'.
        continue
      
      # To improve performance, optionally mark the image as not writeable to
      # pass by reference.
      
      # font
      font = cv2.FONT_HERSHEY_SIMPLEX
        
      # org
      org = (50, 50)
        
      # fontScale
      fontScale = 1
         
      # Blue color in BGR
      color = (255, 255, 255)
        
      # Line thickness of 2 px
      thickness = 2
      
      curr_time = datetime.datetime.now()
      time_str = curr_time.strftime("%H:%M:%S")
      # Using cv2.putText() method 
      
      image.flags.writeable = False
      
      image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)    
      results = face_detection.process(image)
      
      
      if(results.detections):
        facecount=len(results.detections)
      else:
        facecount=0
      senconds=time.time()-time0;
      print("Recording..."+str(facecount)+" face detacted at {} seconds.".format(senconds))
      # Draw the face detection annotations on the image.
      image.flags.writeable = True
      image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)
      if results.detections:
        for detection in results.detections:
          mp_drawing.draw_detection(image, detection)
      
      image = cv2.putText(image, time_str, org, font, fontScale, color, thickness, cv2.LINE_AA)
      out.write(image)
      # Flip the image horizontally for a selfie-view display.
      
      
      #report every 1 second
      if lastreporttime<time.time()-1:
        lastreporttime=time.time()
        datetime_str = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        
        
        scale_percent = 20 # percent of original size
        width = int(image.shape[1] * scale_percent / 100)
        height = int(image.shape[0] * scale_percent / 100)
        dim = (width, height)
        small_image = cv2.resize(image, dim, interpolation = cv2.INTER_AREA)
        cv2.imwrite("small_image.png",small_image)
        f = open('small_image.png', 'rb')
        small_image_png=f.read()
        
        report_obj={ 'time' : datetime_str, 'status' : 'recording', 'device' : 'raspberry pi', 'power' : 100, 'locked' : 1, 'faces':facecount, 'starttime': starttime, 'position': time.time()-time0, 'image':base64.b64encode(small_image_png).decode("utf-8")}
        url=server+"report.php?id={}&IMEI={}".format(deviceid,IMEI)
        t1=time.time()
        r=requests.post(url=url,data=json.dumps(report_obj))
        print("xxxx")
        print(time.time()-t1)
        print(url)
        r.encoding = 'utf-8'
        
        print(r.status_code)
        print(r.text)
        #print(base64.b64encode(image)) 
        
        receive=json.loads(r.text)
        if receive['command']=='finish':
          break
      # cv2.imshow('MediaPipe Face Detection', image)
      if cv2.waitKey(5) & 0xFF == 27:
        break
    
  out.release()
  f = open('output.avi', 'rb')
  avi=f.read()
  time1=time.time()
  
  url=server+"upload.php?id={}&IMEI={}&mode=camera&starttime={}&length={}&filesize={}".format(deviceid,IMEI,starttime,time1-time0,len(avi))
  print(url)
  r=requests.get(url=url)
  r.encoding = 'utf-8'
  print(r.text)
  receive=json.loads(r.text)
  fileid=receive['fileid']
  
  url=server+"upload.php?id={}&IMEI={}&mode=camera&fileid={}".format(deviceid,IMEI,fileid)
  r=requests.post(url=url, data=avi)
  r.encoding = 'utf-8'
  print(r.text)
      
cap.release()

