SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

CREATE TABLE `devices` (
  `id` int(11) NOT NULL,
  `IMEI` varchar(50) DEFAULT NULL,
  `registertime` datetime DEFAULT NULL,
  `registermode` varchar(20) DEFAULT NULL,
  `reportip` varchar(20) DEFAULT NULL,
  `reporttime` datetime DEFAULT NULL,
  `reportstatus` varchar(20) DEFAULT NULL,
  `reportdevice` varchar(20) DEFAULT NULL,
  `reportpower` int(11) DEFAULT NULL,
  `reportlocked` tinyint(1) DEFAULT NULL,
  `reportsound` varchar(200) DEFAULT NULL,
  `reportimage` text,
  `reportstarttime` datetime DEFAULT NULL,
  `reportposition` float DEFAULT NULL,
  `reportfaces` int(11) DEFAULT NULL,
  `reportfileid` int(11) DEFAULT NULL,
  `logintime` datetime DEFAULT NULL,
  `loginuid` int(11) DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL,
  `record` tinyint(1) DEFAULT '0',
  `url` varchar(255) DEFAULT NULL,
  `fileid` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `device_files` (
  `id` int(11) NOT NULL,
  `deviceid` int(11) NOT NULL,
  `uid` int(11) DEFAULT NULL,
  `title` varchar(200) DEFAULT NULL,
  `mode` varchar(20) DEFAULT NULL,
  `starttime` datetime DEFAULT NULL,
  `length` float DEFAULT NULL,
  `filesize` int(11) DEFAULT NULL,
  `filedescription` varchar(200) DEFAULT NULL,
  `path` varchar(250) DEFAULT NULL,
  `marktime` datetime DEFAULT NULL,
  `uploadtime` datetime DEFAULT NULL,
  `savetime` datetime DEFAULT NULL,
  `converttime` datetime DEFAULT NULL,
  `convertstatus` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `device_reports` (
  `id` int(11) NOT NULL,
  `deviceid` int(11) DEFAULT NULL,
  `reportip` varchar(20) DEFAULT NULL,
  `reporttime` datetime DEFAULT NULL,
  `reportstatus` varchar(20) DEFAULT NULL,
  `reportdevice` varchar(20) DEFAULT NULL,
  `reportpower` int(11) DEFAULT NULL,
  `reportlocked` tinyint(1) DEFAULT NULL,
  `reportsound` varchar(255) DEFAULT NULL,
  `reportstarttime` datetime DEFAULT NULL,
  `reportposition` float DEFAULT NULL,
  `reportfaces` int(11) DEFAULT NULL,
  `logintime` datetime DEFAULT NULL,
  `loginuid` int(11) DEFAULT NULL,
  `command` varchar(20) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `fileid` int(11) DEFAULT NULL,
  `servertime` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `user_classes` (
  `id` int(11) NOT NULL,
  `name` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `user_groups` (
  `id` int(11) NOT NULL,
  `type` int(11) DEFAULT NULL,
  `classid` int(11) DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL,
  `pass` varchar(50) DEFAULT NULL,
  `deviceid` int(11) DEFAULT NULL,
  `nickname` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `user_groupstudent` (
  `id` int(11) NOT NULL,
  `groupid` int(11) NOT NULL,
  `studentid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `user_students` (
  `id` int(11) NOT NULL,
  `classid` int(11) NOT NULL,
  `number` varchar(10) DEFAULT NULL,
  `name` varchar(20) NOT NULL,
  `sex` tinyint(1) NOT NULL,
  `birthday` date DEFAULT NULL,
  `pass` varchar(20) DEFAULT NULL,
  `avatar` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `devices`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `device_files`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `device_reports`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `user_classes`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `user_groupstudent`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `user_students`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `device_files`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3169;

ALTER TABLE `device_reports`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24385034;

ALTER TABLE `user_classes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

ALTER TABLE `user_groupstudent`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=119;

ALTER TABLE `user_students`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=119;
COMMIT;
