<?php
namespace {
//die('{"error":"摄像设备没有绑定分组"}');
	date_default_timezone_set("Asia/Shanghai");
	define('TimeZoneSeconds',28800); //东八区

	define('adminroot','/');
	define('temproot',realpath(__DIR__.'/../').'/temp/');
	define('uploadroot',realpath(__DIR__.'/../').'/web/upload/');
	define('uploadurl','http://'.$_SERVER['HTTP_HOST'].'/upload/');
	define('uploadurladmin','http://'.$_SERVER['HTTP_HOST'].'/upload/');
	
	
// 	define('defaultsmtpserver','');

	ini_set('session.cookie_lifetime', '72000');
    session_start();

	try {
		//$conn = new PDO('sqlite:'.realpath(__DIR__.'/../db/itis.s3db'));
        $conn = new PDO('mysql:host=localhost;dbname=itis','itis','itis');
        $conn->exec('set names utf8');
	}
	catch( PDOException $e ) {
		die("Error connecting to SQL Server".$e->getMessage());
	}
	require_once 'current.php';
	require_once 'dataset.php';

	function ctimelength($seconds){

		$hours = intval($seconds/3600);
		$minutes = intval(($seconds - $hours*3600)/60);
		$seconds = $seconds-$hours*3600-$minutes*60;
		$time='';

		if($hours>0) $time.=$hours.'小时';
		if($minutes>0) $time.=$minutes.'分';
		if($seconds>0) $time.=$seconds.'秒';
		if($time=='') $time='0小时';
		return $time;
	}
	function ctime($seconds){
		$hours = intval($seconds/3600);
		$minutes = intval(($seconds - $hours*3600)/60);
		//$seconds = $seconds-$hours*3600-$minutes*60;
		$time=sprintf("%02d",$hours).':'.sprintf("%02d",$minutes);
		return $time;
	}
    function cdate($format,$date='now'){
		if($date) { 
		    $date = new DateTime($date);
		    return $date->format($format);
		}
		else return '';
	}
	function iswechat()	{//MicroMessenger 
		return strpos($_SERVER['HTTP_USER_AGENT'], 'MicroMessenger') !==false;
	}
	function isapple()	{//iPhone
		return (strpos($_SERVER['HTTP_USER_AGENT'], 'iPhone') !==false) || strpos($_SERVER['HTTP_USER_AGENT'], 'iPad') !==false;
	}
	
	function isphone(){
	    $u=$_SERVER['HTTP_USER_AGENT'];
	    if( stristr($u,'ipad') ) {
	        return true;
	    } else if( stristr($u,'iphone') ) {
	        return true;
	    } else if( stristr($u,'blackberry') ) {
	        return true;
	    } else if( stristr($u,'android') ) {
	        return true;
	    } else
	        return false;
	}
	
	
	function isdate($date, $formats = array("Y-m-d", "Y/m/d", "Y-n-j", "Y-m-j", "Y-n-d","Y-m-d H:i:s")) {
	    foreach ($formats as $format) {
	        $d = DateTime::createFromFormat($format, $date);
	        if ( $d && $d->format($format) == $date ) return true;
	    }
	    return false;
	}
	
	function isemail($email) {
		return filter_var($email, FILTER_VALIDATE_EMAIL);
	}
	function isbademail($email) {
		$pattern=str_replace('.', '\\.', '/@(yahoo.cn)|(yahoo.com.cn)|(info.chemsoc.org.cn)$/i');
		return preg_match($pattern, $email);;
	}
	function dieapp($msg) {
	    ob_clean();
	    die('{"error":"'.$msg.'"}');
	}
	function dieback($msg) {
		ob_clean();
		die('<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
	<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	</head>
	<body><script>alert("'.$msg.'");history.back();</script></body>
	</html>');
	}
	function dieclose($msg) {
		ob_clean();
		die('<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
	<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	</head>
	<body><script>alert("'.$msg.'");window.close();</script></body>
	</html>');
	}
	function diewxclose($msg) {
		ob_clean();
		die('<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
	<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	</head>
	<body><script>
			alert("'.$msg.'");
			if (typeof WeixinJSBridge == "undefined"){
		    if( document.addEventListener ){
		        document.addEventListener("WeixinJSBridgeReady", closewindow, false);
		    }else if (document.attachEvent){
		        document.attachEvent("WeixinJSBridgeReady", closewindow);
		        document.attachEvent("onWeixinJSBridgeReady", closewindow);
		    }
			}else{
			    jsApiCall();
			}
			function closewindow() {WeixinJSBridge.call("closeWindow");}
			</script>
	</body>
	</html>');
	}
	function diegoto($msg,$url) {
		ob_clean();
		if($msg)
			die('<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
	<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	</head>
	<body><script>'.($msg?'alert("'.$msg.'");':'').'location.replace("'.$url.'");</script></body>
	</html>');
		else {
		    die('<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
	<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	</head>
	<body><script>location.replace("'.$url.'");</script></body>
	</html>');exit;
			header('HTTP/1.1 303 See Other');
    		header('Location: '.$url);
			exit;
		}
	}
	function getHTTPS($url) {
	    $curl = curl_init(); // 启动一个CURL会话
	    curl_setopt($curl, CURLOPT_URL, $url);
	    curl_setopt($curl, CURLOPT_HEADER, 0);
	    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
	    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false); // 跳过证书检查
	    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2);  // 从证书中检查SSL加密算法是否存在
	    $tmpInfo = curl_exec($curl);   
	    //关闭URL请求
	    curl_close($curl);
	    return $tmpInfo;
	}

	function getstr() {
		$args = func_get_args();
		$count=0;
		foreach($args as $arg)
			if(isset($_GET[$arg])) {
				$GLOBALS[$arg]=$_GET[$arg];
				$count++;
			}
		else
			$GLOBALS[$arg]='';
		return $count;
	}
	function getint() {
		$args = func_get_args();
		$count=0;
		foreach($args as $arg)
			if(isset($_GET[$arg])) {
				$GLOBALS[$arg]=(int)$_GET[$arg];
				$count++;
			}
		else
			$GLOBALS[$arg]=0;
		return $count;
	}
	function getintnull() {
		$args = func_get_args();
		$count=0;
		foreach($args as $arg)
			if(isset($_GET[$arg])&&$_GET[$arg]!='') {
				$GLOBALS[$arg]=(int)$_GET[$arg];
				$count++;
			}
		else
			$GLOBALS[$arg]=null;
		return $count;
	}
	function postint() {
		$args = func_get_args();
		$count=0;
		foreach($args as $arg)
			if(isset($_POST[$arg])) {
				$GLOBALS[$arg]=(int)$_POST[$arg];
				$count++;
			}
		else
			$GLOBALS[$arg]=0;
		return $count;
	}
	function postintnull() {
	    $args = func_get_args();
	    $count=0;
	    foreach($args as $arg)
	        if(isset($_POST[$arg])&&$_POST[$arg]!='') {
	            $GLOBALS[$arg]=(int)$_POST[$arg];
	            $count++;
	        }
	    else
	        $GLOBALS[$arg]=null;
	        return $count;
	}
	function poststr() {
		$args = func_get_args();
		$count=0;
		foreach($args as $arg)
			if(isset($_POST[$arg])) {
				$GLOBALS[$arg]=$_POST[$arg];
				$count++;
			}
		else
			$GLOBALS[$arg]='';
		return $count;
	}
	function json_decode_no_null($str,$ass=true) {
		$result=json_decode($str,$ass);
		if(is_array($result)) return $result; else return array();		
	}
	function array_filter_not_null($array) {
	    if(is_array($array)&&$array) return array_filter($array); else return array();
	}
	
	
	function keys_no_null($arr) {
		if(is_array($arr)){
			$result=array_keys($arr);
			if(is_array($result)) return $result; else return array();		
		} else
			return array();		
	}
	//获取文件后缀名函数
	function fileext($filename)
	{
		return strtolower(substr(strrchr($filename, '.'), 1));
	}
	function clearfilename($filename)
	{
	    return preg_replace('/[\?\*\/\\<>:"\|\']/','',$filename);
	}
	
	function randstr($length)
	{
		$hash = '';
		$chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$max = strlen($chars) - 1;
		mt_srand((double)microtime() * 1000000);
		for($i = 0; $i < $length; $i++)
		{
		$hash .= $chars[mt_rand(0, $max)];
		}
		return $hash;
	}
	function randnum($length)
	{
		$hash = '';
		$chars = '0123456789';
		$max = strlen($chars) - 1;
		mt_srand((double)microtime() * 1000000);
		for($i = 0; $i < $length; $i++)
		{
		$hash .= $chars[mt_rand(0, $max)];
		}
		return $hash;
	}
	function encryptpass($pass) {
		return md5('szac.cn'.md5($pass).'97010180');
	}
	function encryptccspass($pass) {
	    return strtolower(substr(md5($pass),8,16));
	}
	function arroptionkeys($arr,$value) {
		foreach($arr as $k => $v) {
			echo '<option value="'.$k.'"';
			if(!is_null($value)&&$value==$k) echo ' selected';
			echo '>'.$v.'</option>';
		}
	}
	function user_agent($u){
		if( stristr($u,'ipad') ) {
			return "iPad";
		} else if( stristr($u,'iphone') || strstr($u,'iphone') ) {
			return "iPhone";
		} else if( stristr($u,'blackberry') ) {
			return "Blackberry";
		} else if( stristr($u,'android') ) {
			return "Android";
		} else if( stristr($u,'windows')||stristr($u,'msie') ) {
			return "Windows";
		} else if( stristr($u,'macintosh')) {
			return "Macintosh";
		} else if( stristr($u,'linux') ) {
			return "Linux";
		} else {
			return "Unknown";
		}
	}
	
	function donewfiles($html) {
	    $html=str_ireplace('https://www.chemsoc.org.cn/newfiles/', 'http://img.chemsoc.org.cn/', $html);
	    $html=str_ireplace('"http://www.chemsoc.org.cn', '"', $html);
	    $html=str_ireplace('"/newfiles/', '"http://img.chemsoc.org.cn/', $html);
	}
	
	function curl_file_get_contents($durl){
	    $ch = curl_init();
	    curl_setopt($ch, CURLOPT_URL, $durl);
	    curl_setopt($ch, CURLOPT_TIMEOUT, 5);
	    //curl_setopt($ch, CURLOPT_USERAGENT, _USERAGENT_);
	    //curl_setopt($ch, CURLOPT_REFERER,_REFERER_);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	    $r = curl_exec($ch);
	    curl_close($ch);
	    return $r;
	}
	
	function jpgcompress($filename,$maxsize=800,$maxfilesize=500000) {
	    // 内容类型
	    if(!in_array(fileext($filename),array('jpg','jpeg'))) return true;
	    // 获取新的尺寸
	    $size=getimagesize($filename);
	    if(!$size) return false;
	    list($width, $height) = $size;
	    
	    if($width<$maxsize&&$height<$maxsize) return true;
	    if(filesize($filename)<$maxfilesize) return true;
	    
	    if($width>$height) {
	        $new_width=$maxsize;
	        $new_height=$maxsize/$width*$height;
	    } else {
	        $new_height=$maxsize;
	        $new_width=$maxsize/$height*$width;
	    }
	    
	    
	    // 重新取样
	    $image_p = imagecreatetruecolor($new_width, $new_height);
	    $image = imagecreatefromjpeg($filename);
	    if(!$image) return false;
	    imagecopyresampled($image_p, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
	    // 输出
	    return imagejpeg($image_p, $filename, 100);
	}
	function split_name($name) {
	    $name = trim($name);
	    $last_name = (strpos($name, ' ') === false) ? '' : preg_replace('#.*\s([\w-]*)$#', '$1', $name);
	    $first_name = trim( preg_replace('#'.$last_name.'#', '', $name ) );
	    return array($first_name, $last_name);
	}
	function json_unicode_encode($str){
	    //split word
	    preg_match_all('/./u',$str,$matches);
	    
	    $unicodeStr = "";
	    foreach($matches[0] as $m){
	        //拼接
	        $unicodeStr .= "\\u".base_convert(bin2hex(iconv('UTF-8',"UCS-4",$m)),16,16);
	    }
	    return $unicodeStr;
	}
	function countarray($arr) {
	    if(is_array($arr)) return count($arr); else return 0;
	}
	function number2chinese($num)
	{
	    if (is_int($num) && $num < 100) {
	        $char = array('零', '一', '二', '三', '四', '五', '六', '七', '八', '九');
	        $unit = ['', '十', '百', '千', '万'];
	        $return = '';
	        if ($num < 10) {
	            $return = $char[$num];
	        } elseif ($num%10 == 0) {
	            $firstNum = substr($num, 0, 1);
	            if ($num != 10) $return .= $char[$firstNum];
	            $return .= $unit[strlen($num) - 1];
	        } elseif ($num < 20) {
	            $return = $unit[substr($num, 0, -1)]. $char[substr($num, -1)];
	        } else {
	            $numData = str_split($num);
	            $numLength = count($numData) - 1;
	            foreach ($numData as $k => $v) {
	                if ($k == $numLength) continue;
	                $return .= $char[$v];
	                if ($v != 0) $return .= $unit[$numLength - $k];
	            }
	            $return .= $char[substr($num, -1)];
	        }
	        return $return;
	    }
	}
}