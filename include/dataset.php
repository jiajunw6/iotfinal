<?php
//need to be require_once  in 'config.php';
namespace {
	dataset::$conn=$conn;
	abstract class dataset
	{
	    public static $conn;
	    
	    protected static $_cache=array();
	    protected static $_columns=array();
	    protected static $_extends=array();
	    
	    protected static $_table='';
	    
	    protected $data=array();
	    protected $newdata=array(); 
	    
	    function __construct()
	    {
	        $this->initialize();
	    }
	
	    function __destruct()
	    {}
	    
	    public function initialize()
	    {
	        $this->newdata=array();
	        $this->data=array();
	    }
	    
	    public function __set($name, $value)
	    {
	        if(count(static::$_columns)==0||in_array($name,static::$_columns))
	            $this->newdata[":$name"]=$this->data[$name] = $value;
	    }
	    
	    public function __get($name)
	    {
	        if (array_key_exists($name, $this->data))
	            return $this->data[$name];
	        else
	            return null;
	    }
	    
	    public function __isset($name)
	    {
	        return isset($this->data[$name]);
	    }
	    
	    
	    public function __unset($name)
	    {
	        unset($this->data[$name]);
	        unset($this->newdata[":$name"]);
	    }
	    
	    public function load($id,$cols='*')
	    {
	        $this->initialize();
	        if(!$id) return false;
	        $sql="SELECT $cols FROM `".static::$_table."` where `id`=:id";
	        $stmt = static::$conn->prepare($sql);
	        $stmt->execute(array(':id' => $id));
	        if(!($this->data = $stmt->fetch( PDO::FETCH_ASSOC ))) {
	            $this->initialize();
	            return false;
	        }
	        else {
	            return $this->data['id'];
	        }
	    }
	    
	    public function loadPK($PK,$id,$cols='*') {
	        $sql="SELECT `id` FROM `".static::$_table."` where `{$PK}`=:id limit 1";
	        $stmt = static::$conn->prepare($sql);
	        $stmt->execute(array(':id' => $id));
	        if($getid = $stmt->fetchcolumn()) {
	            return $this->load($getid,$cols);
	        } else {
	            $this->initialize();
	            return false;
	        }
	    }
	    
	    public function extend($extends = array()) { //拓展关联的项目
	        if(!$extends) $extends=static::$_extends;
	        foreach($extends as $e) {
	            $ename=substr(strrchr($e, '\\'), 1);
	            $eid=$ename.'id';
	            $this->data[$ename]=new $e($this->$eid);
	        } 
	    }
	    
	    public function refresh()
	    {
	        if(isset($this->data['id'])) return $this->load($this->data['id']); else return false;
	    }
	    
	    public function update()
	    {
	        if(!isset($this->data['id'])) return false;
	        if(count($this->newdata)==0) return $this->data['id']; //û����Ҫ���µ�����
	        try
	        {
	            self::$conn->beginTransaction();    
	            foreach ($this->newdata as $key => $value)
	            {
	                $stmt = static::$conn->prepare("update `".static::$_table."` set `".substr($key,1)."`=$key where `id`={$this->data['id']}");
	                if(!$stmt->execute(array($key => $value))) throw new Exception('dataset update error '.static::$_table."{$this->data['id']} at {$key} = {$value}");
	            }
	            static::$conn->commit();
	            $this->newdata=array();
	            return $this->data['id'];
	        }
	        catch (Exception $e)
	        {
	            echo $e->getMessage();
	            static::$conn->rollback();
	            return false;
	        }
	    }
	    
	    public function delete()
	    {
	    	if(!isset($this->data['id'])) return false;
	    	$stmt = static::$conn->exec("delete from `".static::$_table."` where `id`={$this->data['id']}");
	    }
	    
	    public function insert()
	    {
	        try
	        {
	        	//if(isset($this->newdata[':id'])) unset($this->newdata[':id']);
	            $values=implode(",",array_keys($this->newdata));
	            $columns=str_replace(':','',$values);
	            $stmt = self::$conn->prepare("insert into `".static::$_table."` ($columns) values ($values)");
	            if(!$stmt->execute($this->newdata)) throw new Exception("dataset insert error ".static::$_table);
	            return $this->load(static::$conn->lastInsertId());
	        }
	        catch (Exception $e)
	        {
	            echo $e->getMessage();
	            return false;
	        }
	    }
	    
	    public function copydataforinsert() {
	        $this->newdata=array();
	        foreach($this->data as $key=>$value)
	            $this->newdata[':'.$key]=$value;	        
	    }
	    
	    public function getpost($key,$type='s',$default='') {
	    	if(!($key)) return false;
	    	if($type=='s')
	    		return $this->$key=isset($_POST[$key])&&$_POST[$key]!=''?$_POST[$key]:$default;
	    	else if($type=='trim()')
	    		return $this->$key=isset($_POST[$key])&&$_POST[$key]!=''?trim($_POST[$key]):$default;
	    	else if($type=='trim(not null)')
	    		return $this->$key=isset($_POST[$key])&&$_POST[$key]!=''?trim($_POST[$key]):dieback($default);
	    	else if($type=='n')
	    		return $this->$key=isset($_POST[$key])&&$_POST[$key]!=''?$_POST[$key]:NULL;
	    	else if($type=='d')
	    		return $this->$key=isset($_POST[$key])&&$_POST[$key]!=''?(int)$_POST[$key]:(int)$default;	
	    }
	    public function getpostarr($i,$key,$type='s',$default='') {
	    	if(!($key)) return false;
	    	if($type=='s')
	    		return $this->$key=isset($_POST[$key][$i])&&$_POST[$key][$i]!=''?$_POST[$key][$i]:$default;
	    	else if($type=='trim()')
	    		return $this->$key=isset($_POST[$key][$i])&&$_POST[$key][$i]!=''?trim($_POST[$key][$i]):$default;
	    	else if($type=='n')
	    		return $this->$key=isset($_POST[$key][$i])&&$_POST[$key][$i]!=''?$_POST[$key][$i]:NULL;
	    	else if($type=='d')
	    		return $this->$key=isset($_POST[$key][$i])&&$_POST[$key][$i]!=''?(int)$_POST[$key][$i]:(int)$default;
	    }
	    
	    public function hasitem($conditions)
	    {
	    	$sql="SELECT 1 FROM `".static::$_table."` where $conditions limit 1";
	    	$stmt = static::$conn->query($sql);
	    	return $stmt&&($stmt->rowCount());
	    }
	    
	    public static function options($key,$value) {
	        if($value&&is_array($value)) {
    	        foreach(static::$$key as $v) {
    	            echo '<option value="'.$v.'"';
    	            if(in_array($v,$value)) echo ' selected';
    	            echo '>'.$v.'</option>';
    	        }
	        } else {
	            foreach(static::$$key as $v) {
	                echo '<option value="'.$v.'"';
	                if($value==$v) echo ' selected';
	                echo '>'.$v.'</option>';
	            }
	        }
	    }
	    public static function optionkeys($key,$value) {
	        if($value&&is_array($value)) {
    	        foreach(static::$$key as $k => $v) {
    	            echo '<option value="'.$k.'"';
    	            if(in_array($k,$value)) echo ' selected';
    	            echo '>'.$v.'</option>';
    	        }
	        } else {
	            foreach(static::$$key as $k => $v) {
	                echo '<option value="'.$k.'"';
	                if(!is_null($value)&&$value==$k) echo ' selected';
	                echo '>'.$v.'</option>';
	            }
	        }
	    }
	    
	    public static function prostr($pro,$key) {
	        $pro=$pro.'strs';
	        if(!isset(static::$$pro)) return ''; else $p=static::$$pro;
	        if(isset($p[$key])) return $p[$key]; else return '';
	    }
	    
	    public static function getname($id) {
	        $col='name';
	        if(!isset(static::$_cache[static::$_table.'.'.$col][$id])) {
	            $stmt = static::$conn->query('SELECT `'.$col.'` FROM '.static::$_table.' where `id`='.(int)$id);
	            return static::$_cache[static::$_table.'.'.$col][$id]=$stmt->fetchColumn();
	        } else
	            return static::$_cache[static::$_table.'.'.$col][$id];
	    }
	    public static function gettitle($id,$short='') {
	        $col='title';
	        if(!isset(static::$_cache[static::$_table.'.'.$col][$id])) {
	            $stmt = static::$conn->query('SELECT `'.$col.'` FROM '.static::$_table.' where `id`='.(int)$id);
	            return static::$_cache[static::$_table.'.'.$col][$id]=$stmt->fetchColumn();
	        } else
	            return static::$_cache[static::$_table.'.'.$col][$id];
	    }
	    public static function getemail($id) {
	        $col='email';
	        if(!isset(static::$_cache[static::$_table.'.'.$col][$id])) {
	            $stmt = static::$conn->query('SELECT `'.$col.'` FROM '.static::$_table.' where `id`='.(int)$id);
	            return static::$_cache[static::$_table.'.'.$col][$id]=$stmt->fetchColumn();
	        } else
	            return static::$_cache[static::$_table.'.'.$col][$id];
	    }
	    
	}
	
	
	class littledataset extends \dataset
	{
	
	}
}
?>