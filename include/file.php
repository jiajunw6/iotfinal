<?php
namespace {
	use \PDO as PDO;

	class file extends \dataset
	{
	    //重写原有值
	    protected static $_table="general_files";
	 
	    
	    function __construct()
	    {
	        parent::__construct();
	        $args = func_get_args();
	        $i = count($args);
	        if($i==1) $this->load($args[0]);
	        else if($i==2) $this->load($args[0],$args[1]);
	    }
	    
	    public function load($id,$cols='*') //如果valid=0就丢弃这个记录
	    {
	        $result=parent::load($id,$cols);
	        if(isset($this->data['valid'])&&$this->data['valid']=0) {
	            $this->initialize();
	            return false;
	        } else
	            return $result;
	    }
	    
		public function delete()
	    {
	        if(!isset($this->data['id'])) return false;
	        $stmt = static::$conn->exec("update `".static::$_table."` set `valid`=0 where `id`={$this->data['id']}");
	    	if(file_exists(uploadroot.$this->path)) unlink(uploadroot.$this->path);
	    }
	    
	    public function geticon(){
	    	$exts=array('doc','docx','xls','xlsx','pdf','rar','zip','7z','jpg','png','gif','mp3','txt');
	    	if(in_array($this->ext, $exts))
	    		return uploadurl.'public/icons/'.$this->ext.'.png';
	    	else
	    		return uploadurl.'public/icons/unknow.png';
	    }
	    
	    public static function pasteimagedata($str,$moduleid=2,$adminid=0)
	    {
	        $str=preg_replace_callback('/src="data:image\/(jpg|png|jpeg|gif);base64,([^"]+)"/',function($matches) use($moduleid,$adminid){
	            if(!in_array($matches[1],['jpg','png','jpeg','gif'])||!($data=base64_decode($matches[2]))) return $matches[0]; //转换失败
	            $file=new static();
	            $file->moduleid=$moduleid;
	            $file->adminid=$adminid;
	            $file->ext=$matches[1];
	            $file->origin='base64.'.$file->ext;
	            $file->size=strlen($data);
	            $file->ip=$_SERVER["REMOTE_ADDR"];
	            $file->insert();
	            $file->path='web/base64/id'.$file->id.'_'.randstr(8).'.'.$file->ext;
	            $file->update();
	            file_put_contents(uploadroot.$file->path,$data);
	            $file->update();
	            return 'src="'.uploadurl.$file->path.'"';
	        },$str);
	        
	        $str=preg_replace_callback('/src="(https|http):\/\/(mmbiz.qpic.cn\/mmbiz_jpg|mmbiz.qlogo.cn\/mmbiz_jpg|image2.135editor.com\/cache\/remote)\/([^\?"]+)([^"]+)"/',function($matches) use($moduleid,$adminid){
	          //  echo $matches[1].'://'.$matches[2].'/'.$matches[3];
	          $data=file_get_contents($matches[1].'://'.$matches[2].'/'.$matches[3]);
	          //var_dump($data);
	          
	                $file=new static();
	                $file->moduleid=$moduleid;
	                $file->adminid=$adminid;
	                $file->ext='jpg';
	                $file->origin='wxpic.'.$file->ext;
	                $file->size=strlen($data);
	                $file->ip=$_SERVER["REMOTE_ADDR"];
	                $file->insert();
	                $file->path='web/wxpic/id'.$file->id.'_'.randstr(8).'.'.$file->ext;
	                $file->update();
	                file_put_contents(uploadroot.$file->path,$data);
	                $file->update();
	                return 'src="'.uploadurl.$file->path.'"';
	            },$str);
	            return $str;
	            
	            
	    }
	   
	}
	
	if(isset($_POST['html'])) $_POST['html']=file::pasteimagedata($_POST['html'],2,0);
	if(isset($_POST['enhtml'])) $_POST['enhtml']=file::pasteimagedata($_POST['enhtml'],2,0);

}
?>