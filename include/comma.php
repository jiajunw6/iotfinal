<?php
class comma
{
	public $delimiter=',';
	public $str=array();
	
	function __construct($str='',$d=',')
	{
		$this->delimiter=$d;
		$this->from($str);
		
	}
	public function __toString()
	{
		return $this->to();
	}
	
	public function from($str)
	{
	    if(is_array($str)) $this->data=$str; else $this->data=explode($this->delimiter,$str);	
		return $this;
	}
	
	public function to()
	{
		sort($this->data);
		return implode($this->delimiter,array_unique($this->data));
	}
	
	public function has($s)
	{
		return in_array($s,$this->data);
	}
	
	public function ins($s)
	{
		if(!in_array($s,$this->data)) $this->data[]=$s;
		return $this;
	}
	public function del($s)
	{
		array_unique($this->data);
		$pos=array_search($s,$this->data);
		if($pos!==false) unset($this->data[$pos]);
		return $this;
	}
}

 
//echo (new comma('1,2,3,4,5,6,7,9'))->ins(8)->ins(10)->ins(11)->ins(12)->del(1);
?>