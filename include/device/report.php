<?php
namespace device {
	use \PDO as PDO;

	class report extends \dataset
	{
	    //重写原有值
	    protected static $_table="device_reports";
	    
	    function __construct()
	    {
	        parent::__construct();
	        $args = func_get_args();
	        $i = count($args);
	        if($i==1) $this->load($args[0]);
	        else if($i==2) $this->load($args[0],$args[1]);
	    }
	    
	}

}
?>