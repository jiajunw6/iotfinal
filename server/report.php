<?php
require_once __DIR__.'/../include/config.php';
require_once __DIR__.'/../include/device/device.php';
require_once __DIR__.'/../include/device/report.php';
require_once __DIR__.'/../include/user/student.php';

use \device\device as device;
use \device\report as report;
use \user\student as student;

getint('id'); getstr('IMEI');

$device=new device(); $report=new report();

if(!$device->load($id)) dieapp('设备ID不正确');

$postdata = file_get_contents("php://input");
$data = json_decode($postdata);
if(!$data)  dieapp('JSON格式不正确');

$command=$defaultcommand;

//$device->IMEI=$IMEI;
$device->reportip=$_SERVER['REMOTE_ADDR'];

if(strtotime('now')-strtotime($device->reporttime)>10) {
    //10s clear old data
    $device->reportstarttime=null;
    $device->reportposition=null;
    $device->reportimage=null;
    $device->reportsound=null;
}



$device->reporttime=$data->time;
$device->reportstatus=$data->status;

$device->reportdevice=$data->device;
$device->reportpower=$data->power;
$device->reportlocked=$data->locked;

if($device->reportstatus=='recording') {

    if(isset($data->starttime)) {$device->reportstarttime=$data->starttime; $report->reportstarttime=$device->reportstarttime;}
    if(isset($data->position)) {$device->reportposition=$data->position; $report->reportposition=$device->reportposition;}
    if(isset($data->image)&&$data->image) {$device->reportimage=$data->image;}
    if(isset($data->sound)&&$data->sound) {$device->reportsound=$data->sound; $report->reportsound=$device->reportsound;}
    if(isset($data->faces)) {$device->reportfaces=$data->faces; $report->reportfaces=$device->reportfaces;}
    $device->update();
    
    
    if($device->record) $command='continue'; else $command='finish';
    $report->deviceid=$device->id;
    $report->reportip=$device->reportip;
    $report->reporttime=$device->reporttime;
    $report->reportstatus=$device->reportstatus;
    $report->reportdevice=$device->reportdevice;
    $report->reportpower=$device->reportpower;
    $report->reportlocked=$device->reportlocked;


    $report->logintime=$device->logintime;
    $report->loginuid=$device->loginuid;
    $report->command=$command;
    $report->url=null;
    $report->fileid=null;
    $report->servertime=cdate('Y-m-d H:i:s');

    //$report->reportjson=$postdata;


    $report->insert();
    
    $output=[
        'time'=>cdate('Y-m-d H:i:s.u'),
        'command'=>$command,
        'login'=>1
    ]; 
    
} else if($device->reportstatus=='waiting') {
    
    if($device->loginuid||$noneedlogin) $login=1; else $login=0; 
    
    //有record最优先，然后url浏览再处理，没有再处理上传
    $url=null; $fileid=null;
    if($device->record) {
        $command='record';
    } else if($device->url) {
        $url=$device->url;
        if(fileext($device->url)=='apk') $command='update'; else $command='browser';
        $device->url=null;
    } else if($device->fileid) {$fileid=$device->fileid; $command='upload'; $device->fileid=null;}
        
    $device->update();
    
    $report->deviceid=$device->id;
    $report->reportip=$device->reportip;
    $report->reporttime=$device->reporttime;
    $report->reportstatus=$device->reportstatus;
    $report->reportdevice=$device->reportdevice;
    $report->reportpower=$device->reportpower;
    $report->reportlocked=$device->reportlocked;
    $report->reportsound=$device->reportsound;
    $report->reportstarttime=$device->reportstarttime;
    $report->reportposition=$device->reportposition;
    $report->logintime=$device->logintime;
    $report->loginuid=$device->loginuid;
    $report->command=$command;
    $report->url=$url;
    $report->fileid=$fileid;
    $report->servertime=cdate('Y-m-d H:i:s.u');
    $report->insert();

    $output=[
        'time'=>cdate('Y-m-d H:i:s.u'),
        'title'=>$device->title,
        'command'=>$command,
        'login'=>$login
    ];

    if($url) $output['url']=$url;
    if($fileid) $output['fileid']=$fileid;


}

/*
$output=[
    'time'=>cdate('Y-m-d H:i:s.u'),
    'title'=>'xxxx',
    'command'=>'none',
    'login'=>0
];
*/

echo json_encode($output,JSON_UNESCAPED_UNICODE);
//echo cdate('Y-m-d H:i:s.u');
