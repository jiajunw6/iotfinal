<?php
require_once __DIR__.'/../include/config.php';
require_once __DIR__.'/../include/device/device.php';
require_once __DIR__.'/../include/user/group.php';
require_once __DIR__.'/../include/user/student.php';

use \device\device as device;
use \user\student as student;
use \user\group as group;

getint('id','uid'); getstr('IMEI');

$device=new device(); $group=new group(); $student=new student();

if(!$device->load($id)) dieapp('设备ID不正确');

$device->IMEI=$IMEI;
$device->reportip=$_SERVER['REMOTE_ADDR'];
$device->registertime=cdate('Y-m-d H:i:s.u');
$device->reporttime=cdate('Y-m-d H:i:s.u');
$device->reportstatus='register';
if($device->id<0) {

    $sql = "select `id`,`name` from `user_groups` where `type`={$currentgrouptype} and `classid`={$currentclass} and `deviceid`={$device->id}";
    $bindgroup = $conn->query($sql)->fetch(PDO::FETCH_OBJ);
    //if($_SERVER['REMOTE_ADDR']=='192.168.99.2')  var_dump($conn->query("select * from `user_groups`")->fetch());
    if(!$uid&&!$bindgroup&&$device->loginuid) $uid=$device->loginuid;
    if($noneedlogin) {

        $device->registermode='camera';
        $device->logintime=cdate('Y-m-d H:i:s.u');
        $device->loginuid=0;
        $device->title='免登录模式';
        $device->update();

        $output=[
            'time'=>cdate('Y-m-d H:i:s.u'),
            'mode'=>'camera',
            'title'=>$device->title,
            'login'=>1,
            'userlist'=>[]
        ];

    } else if($bindgroup || $group->load(-$uid)) {

        if($bindgroup) $group=$bindgroup;
        //login success
        $device->registermode='camera';
        $device->title=$group->name;
        $device->logintime=cdate('Y-m-d H:i:s.u');
        $device->loginuid=-$group->id;
        $device->update();

        $output=[
            'time'=>cdate('Y-m-d H:i:s.u'),
            'mode'=>'camera',
            'title'=>$device->title,
            'login'=>1,
            'userlist'=>[]
        ];
    } else {
        // userlist
        $device->registermode='camera';
        $device->title='请登录';
        $device->update();
        //要求登录
        $userlist=[];
        $sql="select `id`,`name` from `user_classes` where `id`={$currentclass}";
        $stmt=$conn->query($sql);
        while($class=$stmt->fetch(PDO::FETCH_OBJ)) {
            $groups=[];
            $sql1="select `id`,`name` from `user_groups` where `classid`={$class->id}";
            $stmt1=$conn->query($sql1);
            while($group=$stmt1->fetch(PDO::FETCH_OBJ)) $groups[]=['uid'=>-$group->id,'name'=>$group->name,'pass'=>''];
            $userlist[]=['classname'=>$class->name,'namelist'=>$groups];
        }

        $output=[
            'time'=>cdate('Y-m-d H:i:s.u'),
            'mode'=>'camera',
            'title'=>$device->title,
            'login'=>0,
            'userlist'=>$userlist
        ];
    }
    /*
    $sql="select `id`,`name` from `user_students` `s` 
    where exists(select 1 from `user_groupstudent` `gs` where `gs`.`groupid`={$group->id} and `gs`.`studentid`=`s`.`id`) order by `s`.`id`";
    $stmt=$conn->query($sql);
    $students=[];
    while($student=$stmt->fetch(PDO::FETCH_OBJ)) $students[]=$student->name;
    $students=implode('、',$students);
    */



    
} else {
    //学生登录
    if(!$uid&&$device->loginuid)$uid=$device->loginuid;
    if($noneedlogin) {
        
        $device->registermode='recorder';
        $device->logintime=cdate('Y-m-d H:i:s.u');
        $device->loginuid=0;
        $device->title='免登录模式';
        $device->update();
        
        $output=[
            'time'=>cdate('Y-m-d H:i:s.u'),
            'mode'=>'recorder',
            'title'=>$device->title,
            'login'=>1,
            'userlist'=>[]
        ]; 
        
    } else if(!$grouporstudent&&!$student->load($uid)) {
        
        $device->registermode='recorder';
        $device->title='请登录';
        $device->update();
        //要求登录
        $userlist=[];
        $sql="select `id`,`name` from `user_classes` where `id`={$currentclass}";
        $stmt=$conn->query($sql);
        while($class=$stmt->fetch(PDO::FETCH_OBJ)) {
            $students=[];
            $sql1="select `id`,`name`,`pass` from `user_students` where `classid`={$class->id}";
            $stmt1=$conn->query($sql1);
            while($student=$stmt1->fetch(PDO::FETCH_OBJ)) $students[]=['uid'=>$student->id,'name'=>$student->name,'pass'=>$student->pass];
            $userlist[]=['classname'=>$class->name,'namelist'=>$students];
        }
        
        $output=[
            'time'=>cdate('Y-m-d H:i:s.u'),
            'mode'=>'recorder',
            'title'=>$device->title,
            'login'=>0,
            'userlist'=>$userlist
        ];
        
    } else if($grouporstudent&&!$group->load(-$uid)) {
        
        $device->registermode='recorder';
        $device->title='请登录';
        $device->update();
        //要求登录
        $userlist=[];
        $sql="select `id`,`name` from `user_classes` where `id`={$currentclass}";
        $stmt=$conn->query($sql);
        while($class=$stmt->fetch(PDO::FETCH_OBJ)) {
            $groups=[];
            $sql1="select `id`,`name` from `user_groups` where `classid`={$class->id}";
            $stmt1=$conn->query($sql1);
            while($group=$stmt1->fetch(PDO::FETCH_OBJ)) $groups[]=['uid'=>-$group->id,'name'=>$group->name,'pass'=>''];
            $userlist[]=['classname'=>$class->name,'namelist'=>$groups];
        }
        
        $output=[
            'time'=>cdate('Y-m-d H:i:s.u'),
            'mode'=>'recorder',
            'title'=>$device->title,
            'login'=>0,
            'userlist'=>$userlist
        ];
        
    } else if(!$grouporstudent&&$student->load($uid)){
        
        $sql="select `id` from `devices` where `id`!={$device->id} and `loginuid`={$student->id}";
        if($conflictid=$conn->query($sql)->fetchColumn()) dieapp("用户已经在{$conflictid}号设备登录");
        
        $device->registermode='recorder';
        $device->logintime=cdate('Y-m-d H:i:s.u');
        $device->loginuid=$student->id;
        $device->title=$student->name;
        $device->update();
        
        $output=[
            'time'=>cdate('Y-m-d H:i:s.u'),
            'mode'=>'recorder',
            'title'=>$device->title,
            'login'=>1,
            'userlist'=>[]
        ];
        
    } else if($grouporstudent&&$group->load(-$uid)){
        
        $sql="select `id` from `devices` where `id`>0 and `id`!={$device->id} and `loginuid`={$group->id}";
        if($conflictid=$conn->query($sql)->fetchColumn()) dieapp("用户已经在{$conflictid}号设备登录");
        
        $device->registermode='recorder';
        $device->logintime=cdate('Y-m-d H:i:s.u');
        $device->loginuid=-$group->id;
        $device->title=$group->name;
        $device->update();
        
        $output=[
            'time'=>cdate('Y-m-d H:i:s.u'),
            'mode'=>'recorder',
            'title'=>$device->title,
            'login'=>1,
            'userlist'=>[]
        ];
        
    }
    
}


echo json_encode($output,JSON_UNESCAPED_UNICODE);
//echo cdate('Y-m-d H:i:s.u');