<?php
require_once __DIR__.'/../include/config.php';
require_once __DIR__.'/../include/device/device.php';
require_once __DIR__.'/../include/device/file.php';

use \device\device as device;
use \device\file as file;

getint('id','fileid'); getstr('IMEI','mode','starttime','length','filesize');

file_put_contents('upload.txt',cdate('Y-m-d H:i:s.u').PHP_EOL.json_encode($_GET).PHP_EOL.json_encode($_POST).PHP_EOL.json_encode($_FILES),FILE_APPEND);


$device=new device(); $file=new file();

if(!$device->load($id)) dieapp('设备ID不正确');


if($mode=='camera') $ext='.mp4'; else if($mode=='recorder') $ext='.mp3'; else dieapp('设备mode不正确');

if(!$fileid) {
    if(!isdate(cdate('Y-m-d H:i:s',$starttime))) $starttime=null; else $starttime=cdate('Y-m-d H:i:s',$starttime);
    $sql="select `id` from `device_files` where `deviceid`=:deviceid and (`starttime`)=:starttime and `filesize`=:filesize";
    $stmt=$conn->prepare($sql);
    $stmt->execute([':deviceid'=>$device->id,':starttime'=>$starttime,':filesize'=>$filesize]);
    $fileid=$stmt->fetchColumn();
    
    
    if($fileid) $file->load($fileid);
    $file->mode=$mode;
    $file->deviceid=$device->id;
    $file->uid=$device->loginuid;
    $file->title=$device->title;
    $file->starttime=$starttime;
    $file->length=$length;
    $file->marktime=cdate('Y-m-d H:i:s');
    $file->filesize=$filesize;
    $file->filedescription=cdate('Y-m-d')."[{$file->uid}]{$file->title}";
    //var_dump($file);
    if($file->id) $file->update(); else $file->insert();
    
    $output=[
        'time'=>cdate('Y-m-d H:i:s.u'),
        'fileid'=>$file->id,
        'filedescription'=>$file->filedescription
    ]; 
    
} else if($file->load($fileid)){
    $data = file_get_contents("php://input");
    
    $file->path="[uid$file->uid]devid{$file->deviceid}-fileid{$file->id}-".cdate('YmdHis',$file->starttime).'.avi';
    $file->uploadtime=cdate('Y-m-d H:i:s');
    $file->update();
    $device->reportfileid=$file->id;
    $device->reportimage=null;
    $device->update();
    file_put_contents(uploadroot.$file->path,$data);

    $file->savetime=cdate('Y-m-d H:i:s');
    $file->update();

    $output=[
        'time'=>cdate('Y-m-d H:i:s.u'),
        'fileid'=>$file->id,
        'status'=>'success'
    ]; 
    
} else {
    dieapp('文件处理错误');    
}


echo json_encode($output,JSON_UNESCAPED_UNICODE);
//echo cdate('Y-m-d H:i:s.u');
