<?php
require_once __DIR__.'/../../include/config.php';
require_once __DIR__.'/../../include/dataset.php';


require_once __DIR__.'/template.php';

//取得get参数

getintnull('id','loginuid','record','upload','reset'); getstr('url');

$where=' where 1';
if($id) $where.=' and `id`='.$id;

if(!is_null($loginuid)&&$loginuid==0) {
    //$where.=' and `id`>0'; //只有录音设备才能下线
    $conn->exec("update `devices` set `logintime`=null, `loginuid`=null, `title`='未登录' {$where}");
}

if(!is_null($record)){
    $where.=' and `loginuid`!=0';
    $conn->exec("update `devices` set `record`={$record} {$where}");
}

if(isset($_GET['url'])){
    $conn->exec("update `devices` set `url`='{$url}' {$where}");
}

if($upload){
    //$where.=' and `id`<0';
    $conn->exec("update `devices` set `fileid`=(
        select `id` from `device_files` `f` where `f`.`deviceid`=`devices`.`id` and `marktime`>'2021-03-30' and `savetime` is null order by `id` desc limit 1
    ) {$where} ");
}

if($reset) {
    $conn->exec("update devices set 
        `registertime`=null,
        `registermode`=null,
        `reportip`=null,
        `reporttime`=null,
        `reportstatus`=null,
        `reportdevice`=null,
        `reportpower`=null,
        `reportlocked`=null,
        `reportsound`=null,
        `reportimage`=null,
        `reportstarttime`=null,
        `reportposition`=null,
        `reportfileid`=null,
        `logintime`=null,
        `loginuid`=null,
        `title`=null,
        `record`=null,
        `url`=null,
        `fileid`=null {$where}");
}

    
    
diegoto('','devices.php?s_loginuid=1');
