<?php
require_once __DIR__.'/../../include/config.php';
require_once __DIR__.'/../../include/dataset.php';
require_once __DIR__.'/../../include/device/file.php';


require_once __DIR__.'/template.php';

use \device\file as file;

getintnull('s_loginuid','s_mode','s_err'); getstr();

$page=isset($_GET['page'])?(int)$_GET['page']:1; if($page<1) $page=1;

$where=''; $parameters=array(); $linkarr=array();

if($s_loginuid==1) { $where.=' and `loginuid`!=0'; $linkarr['s_loginuid']=$s_loginuid;}
if(!is_null($s_loginuid)&&$s_loginuid==0) { $where.=' and (`loginuid`=0 or `loginuid` is null)'; $linkarr['s_loginuid']=$s_loginuid;}

if($s_mode>0) { $where.=' and `id`>0'; $linkarr['s_mode']=$s_mode;}
if($s_mode<0) { $where.=' and `id`<0'; $linkarr['s_mode']=$s_mode;}


if($s_err==1) { $where.=" and (
    strftime('%s','now','localtime') - strftime('%s',`reporttime`)>30 or
    (`id`>0 and `reportstatus`=='recording' and `reportdevice`!='earphone') or
    (`id`<0 and `reportstatus`=='recording' and `reportlocked`=1) or
    `reportpower`<30 or
    (`record`=1 and `reportstatus`!='recording') or
    (`record`!=1 and `reportstatus`=='recording')
)

"; $linkarr['s_err']=$s_err;}

$order='(`loginuid`>0 or `loginuid`<0) desc,`loginuid` desc, `id` asc';
$where=' where 1'.$where;
$links=http_build_query($linkarr);

$xlsx_where=base64_encode($where);
$xlsx_parameters=base64_encode(json_encode($parameters));
$xlsx_timestamp=time();
$xlsx_sign=encryptpass(md5($xlsx_where.$xlsx_parameters).$xlsx_timestamp);
$xlsx_where=urlencode($xlsx_where);$xlsx_parameters=urlencode($xlsx_parameters);


$headcode='
<script type="text/javascript" src="function.js"></script>
';
htmlhead('Devices',['form'],$headcode);

?>

				<div class="crumbs">
					<ul id="breadcrumbs" class="breadcrumb">
						<li><i class="icon-home"></i><a href="./">Recording</a></li>
						<li class="current"><a href="#" title="">Devices</a></li>
					</ul>
					<ul class="crumb-buttons"><li><a href="" target="_blank"><i class="icon-wrench"></i><span>Jiajun Wu (jiajunw6)</span></a></li></ul>
				</div>
				<div class="page-header">
					<div class="page-title">
						<h3>Cameras and Recorders</h3>
						<span>Remote control platform</span>
					</div>
				</div>
				<!--=== Page Content ===-->
				<div class="row">
					<div class="col-md-12">
						<div class="widget box">
							<div class="widget-header">
								<h4><i class="icon-search"></i> Filter</h4>
							</div>
							<div class="widget-content">
								<form name="sform" id="sform" class="form-horizontal row-border" action="?" method="get">
									<div class="form-group">
										<label class="col-md-0-1 control-label">Login：</label>
										<div class="col-md-1">
											<select class="form-control" name="s_loginuid">
												<option value="">ALL</option>
												<option value="1"<?php if(!is_null($s_loginuid)&&$s_loginuid==1) echo ' selected="selected"';?>>Yes</option>
												<option value="0"<?php if(!is_null($s_loginuid)&&$s_loginuid==0) echo ' selected="selected"';?>>No</option>
?>
											</select>
										</div>
										<label class="col-md-0-1 control-label">Mode：</label>
										<div class="col-md-1">
											<select class="form-control" name="s_mode">
												<option value="">ALL</option>
												<option value="1"<?php if($s_mode==1) echo ' selected="selected"';?>>Audio Recorder</option>
												<option value="-1"<?php if($s_mode==-1) echo ' selected="selected"';?>>Camera</option>
												
											</select>
										</div>
										<label class="col-md-0-1 control-label">Exception：</label>
										<div class="col-md-1">
											<select class="form-control" name="s_err">
												<option value=""></option>
												<option value="1"<?php if($s_err==1) echo ' selected="selected"';?>>Yes</option>
												
											</select>
										</div>
									</div>
									<div class="form-actions">
											<input type="submit" class="btn btn-sm btn-success" value="Search">
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
				
				<div class="row">
					<!--=== Table Classes ===-->
					<div class="col-md-12">
						<div class="widget box">
							<div class="widget-header">
								<h4><i class="icon-reorder"></i> Devices</h4>
								<div class="toolbar no-padding">
									<div class="btn-group">
										<span class="btn btn-xs widget-collapse"><i class="icon-angle-down"></i></span>
									</div>
								</div>
							</div>
							<div class="widget-content no-padding">
								<table class="table table-hover table-striped table-bordered table-highlight-head">
									<thead>
										<tr>
											<th width="5%">ID</th>
											<th>Mode</th>
											<th>ip</th>
											<th>Identity</th>
											<th>Latest Report</th>
											<th>Status</th>
											<th>Mode</th>
											<th>Battery</th>
											<th>Faces</th>
											<th>Quick Preview</th>
										</tr>
									</thead>
									<tbody>
<?php 
									$pagesize=200;
										
									$sql="SELECT count(`id`) FROM `devices` $where";
									$stmt = $conn->prepare($sql);
									$stmt->execute($parameters);
									$recordcount = (int)$stmt->fetch()[0];
									$pagecount = ceil($recordcount/$pagesize);
									if($page>$pagecount) $page=$pagecount;
									$pagestart=($page-1)*$pagesize;								
										
									$sql="SELECT * FROM `devices` $where order by $order limit $pagestart,$pagesize";
									$stmt = $conn->prepare($sql);
									$stmt->execute($parameters);

									while($stmt&&$arr=$stmt->fetch( PDO::FETCH_ASSOC )) {
										for($i=0;$i<4;$i++)  {
?>
										<tr>
											<td><?php echo $arr['id'];?></td>
											<td><?php echo $arr['registermode'];?></td>
											<td><?php echo $arr['reportip'];?></td>
											<td><?php if($arr['loginuid']) echo '<a class="btn btn-xs btn-default" href="devices_save.php?id='.$arr['id'].'&loginuid=0">'."[{$arr['loginuid']}]</a>".$arr['title']; else echo '<span class="label label-xs label-default">Not Login</span>';
											if($i) echo ' - Simulated Device'; else echo ' - Real Device';
											?></td>
											<td><?php
											if($arr['reporttime']) {
											    if(strtotime('now')-strtotime($arr['reporttime'])>30)
											        echo '<span class="label label-xs label-danger">'.cdate('H:i:s',$arr['reporttime']).'</span>'; 
											    else
											        echo '<span class="label label-xs label-default">'.cdate('H:i:s',$arr['reporttime']).'</span>';
											
											}
											?></td>
											<td><?php
											if($arr['record']) {
											    echo '<a class="btn btn-xs btn-danger" href="devices_save.php?id='.$arr['id'].'&record=0">■</a>';
											} else {
											    echo '<a class="btn btn-xs btn-default" href="devices_save.php?id='.$arr['id'].'&record=1">▶</a>';
											}
											
											if(($arr['reportstatus']=='recording'&&!$arr['record'])||$arr['reportstatus']!='recording'&&$arr['record']) {
											    echo ' <span class="label label-xs label-warning">'.$arr['reportstatus'].'</span>';
											} else 
											    echo ' <span class="label label-xs label-default">'.$arr['reportstatus'].'</span>';
											
											if($arr['reportstatus']=='recording') echo ' <span class="label label-xs label-success">'.$arr['reportposition'].'</span>';
											?></td>
											<td><?php

											    echo $arr['reportdevice'];

											?></td>
											<td><?php
											if($arr['reportpower']) {
											    if($arr['reportpower']<30)
											        echo '<span class="label label-xs label-danger">'.$arr['reportpower'].'%</span>';
											    else
											        echo '<span class="label label-xs label-default">'.$arr['reportpower'].'%</span>';
											
											}
											?></td>
											<td><?php
											    if($arr['id']<0&&$arr['reportstatus']=='recording'&&$arr['reportfaces']<2)
											        echo '<span class="label label-xs label-danger">'.$arr['reportfaces'].'</span>';
											    else
											        echo $arr['reportfaces'];
											
											?></td>
											<td><?php if($arr['reportstatus']=='recording') {
											    if($arr['reportimage']) echo '<img width="200" height="150" src="data:image/png;base64,'.$arr['reportimage'].'"/>';
												    if($arr['reportsound']) {
													$soundarr=json_decode_no_null($arr['reportsound']);
													$soundarr[]=0;
													$sound=max($soundarr);
													if($sound<10) echo '<span class="label label-xs label-danger">'.$sound.'db</span>'; else  echo $sound.'db';
												    } 
											    }else if($arr['reportfileid']) {
											    		$file=new file($arr['reportfileid']);
											    		 echo '<a class="btn btn-sm btn-info" href="http://localhost/upload/'.$file->path.'">Download Video</a>';
											    	
									        	   }
									        ?></td>
										</tr>									
<?php 
										}
									}
?>				
			
									</tbody>
								</table>
								<div class="row">
									<div class="table-footer">
										<div class="col-md-4">
											<div class="table-actions">											
												<a class="btn btn-sm icon-th" href=""><?php echo " $recordcount Devices in total";?></a>
											</div>
										</div>
										<div class="col-md-8">
											<ul class="pagination">
												<li class="<?php if($page==1) echo 'disabled';?>"><a href="<?php if($page>1) echo '?'.$links.'&page=1'; else echo 'javascript:void(0);';?>">First</a></li>
												<li class="<?php if($page==1) echo 'disabled';?>"><a href="<?php if($page>1) echo '?'.$links.'&page='.($page-1); else echo 'javascript:void(0);';?>">&larr; Prev</a></li>
												<li><a href="javascript:gotopage('<?php echo $links;?>');"><?php echo $page;?> / <?php echo $pagecount;?></a></li>										
												<li class="<?php if($page==$pagecount) echo 'disabled';?>"><a href="<?php if($page<$pagecount) echo '?'.$links.'&page='.($page+1); else echo 'javascript:void(0);';?>">Next &rarr;</a></li>
												<li class="<?php if($page==$pagecount) echo 'disabled';?>"><a href="<?php if($page<$pagecount) echo '?'.$links.'&page='.$pagecount; else echo 'javascript:void(0);';?>">Last</a></li>
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- /Table Classes -->
				</div>
				<!-- /.row -->

				<!-- /Page Content -->
				<div class="row">
					<div class="col-md-12">
						<div class="widget box">
							<div class="widget-header">
								<h4><i class="icon-cog"></i> Action</h4>
							</div>
							<div class="widget-content">
								<div class="tabbable box-tabs">
									<ul class="nav nav-tabs">
										<li class="active"><a href="#box_tab1" data-toggle="tab">Record</a></li>
									</ul>
									<div class="tab-content">
										<div class="tab-pane active" id="box_tab1">
        									<form class="form-horizontal" action="devices_save.php?" method="get">
        									<div class="form-group">
        										<label class="col-md-2 control-label">Button：<span class="required">*</span></label>
        										<div class="col-md-6">		
        											<a class="btn btn-danger" href="devices_save.php?record=0" >■ Stop All</a>	
        											<a class="btn btn-success" href="devices_save.php?record=1" >▶ Record All</a>
        											<a class="btn btn-warning" href="devices_save.php?reset=1">Reset ALL</a>
        										</div>
        										<div class="col-md-2">		
        										</div>
        									</div>
											</form>
										</div>
									 
        									
									</div>
								</div> <!-- /.tabbable portlet-tabs -->
							</div> <!-- /.widget-content -->
						</div> <!-- /.widget .box -->
					</div> <!-- /.col-md-12 -->
				</div> <!-- /.row -->
				
<?php
htmlfoot();
