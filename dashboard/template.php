<?php
//这个文件专门存放管理员身份验证模块，和显示模板
//if($_SERVER['REMOTE_ADDR']!='127.0.0.1') die('local');

require_once __DIR__.'/../include/config.php';
require_once __DIR__.'/../include/dataset.php';

function htmlhead($title = "", $js=[], $headcode='',$clear=0)
{
?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0" />
	<title><?php echo $title; ?> - ITIS</title>

	<!--=== CSS ===-->

	<!-- Bootstrap -->
	<link href="<?php echo adminroot; ?>bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />

	<!-- jQuery UI -->
	<!--<link href="plugins/jquery-ui/jquery-ui-1.10.2.custom.css" rel="stylesheet" type="text/css" />-->
	<!--[if lt IE 9]>
		<link rel="stylesheet" type="text/css" href="plugins/jquery-ui/jquery.ui.1.10.2.ie.css"/>
	<![endif]-->

	<!-- Theme -->
	<link href="<?php echo adminroot; ?>assets/css/main.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo adminroot; ?>assets/css/plugins.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo adminroot; ?>assets/css/responsive.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo adminroot; ?>assets/css/icons.css" rel="stylesheet" type="text/css" />

	<link rel="stylesheet" href="<?php echo adminroot; ?>assets/css/fontawesome/font-awesome.min.css">
	<!--[if IE 7]>
		<link rel="stylesheet" href="<?php echo adminroot; ?>assets/css/fontawesome/font-awesome-ie7.min.css">
	<![endif]-->

	<!--=== JavaScript ===-->

	<script type="text/javascript" src="<?php echo adminroot; ?>assets/js/libs/jquery-1.10.2.min.js"></script>
	<script type="text/javascript" src="<?php echo adminroot; ?>plugins/jquery-ui/jquery-ui-1.10.2.custom.min.js"></script>

	<script type="text/javascript" src="<?php echo adminroot; ?>bootstrap/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo adminroot; ?>assets/js/libs/lodash.compat.min.js"></script>

	<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
		<script src="<?php echo adminroot; ?>assets/js/libs/html5shiv.js"></script>
	<![endif]-->

	<!-- Smartphone Touch Events -->
	<script type="text/javascript" src="<?php echo adminroot; ?>plugins/touchpunch/jquery.ui.touch-punch.min.js"></script>
	<script type="text/javascript" src="<?php echo adminroot; ?>plugins/event.swipe/jquery.event.move.js"></script>
	<script type="text/javascript" src="<?php echo adminroot; ?>plugins/event.swipe/jquery.event.swipe.js"></script>

	<!-- General -->
	<script type="text/javascript" src="<?php echo adminroot; ?>assets/js/libs/breakpoints.js"></script>
	<script type="text/javascript" src="<?php echo adminroot; ?>plugins/respond/respond.min.js"></script> <!-- Polyfill for min/max-width CSS3 Media Queries (only for IE8) -->
	<script type="text/javascript" src="<?php echo adminroot; ?>plugins/cookie/jquery.cookie.min.js"></script>
	<script type="text/javascript" src="<?php echo adminroot; ?>plugins/slimscroll/jquery.slimscroll.min.js"></script>
	<script type="text/javascript" src="<?php echo adminroot; ?>plugins/slimscroll/jquery.slimscroll.horizontal.min.js"></script>

	<!-- Bootbox -->
	<script type="text/javascript" src="<?php echo adminroot; ?>plugins/bootbox/bootbox.min.js"></script>
	
	<!-- Globalize -->
	<script type="text/javascript" src="<?php echo adminroot; ?>plugins/globalize/globalize.js"></script>
	<script type="text/javascript" src="<?php echo adminroot; ?>plugins/globalize/cultures/globalize.culture.zh-CN.js"></script>
	
	<!-- App -->
	<script type="text/javascript" src="<?php echo adminroot; ?>assets/js/app.js"></script>
	<script type="text/javascript" src="<?php echo adminroot; ?>assets/js/plugins.js"></script>
	
	<!-- App -->
	<script type="text/javascript" src="<?php echo adminroot; ?>assets/js/szac.js"></script>
	
	<script>
	$(document).ready(function(){
		"use strict";

		App.init(); // Init layout and core plugins
		Plugins.init(); // Init all plugins
	});
	</script>
	
	<?php if(in_array('form',$js)) {?>
	<!-- Forms -->
	<script type="text/javascript" src="<?php echo adminroot; ?>assets/js/plugins.form-components.js"></script>
	<script type="text/javascript" src="<?php echo adminroot; ?>plugins/select2/select2.min.js"></script> <!-- Styled select boxes -->
	<script type="text/javascript" src="<?php echo adminroot; ?>plugins/inputlimiter/jquery.inputlimiter.min.js"></script>
	<script type="text/javascript" src="<?php echo adminroot; ?>plugins/bootstrap-inputmask/jquery.inputmask.min.js"></script>
	<script type="text/javascript" src="<?php echo adminroot; ?>plugins/uniform/jquery.uniform.min.js"></script> <!-- Styled radio and checkboxes -->
	<!-- Form Validation -->
	<script type="text/javascript" src="<?php echo adminroot; ?>plugins/validation/jquery.validate.min.js"></script>
	<script type="text/javascript" src="<?php echo adminroot; ?>plugins/validation/additional-methods.min.js"></script>

	<!-- Form Wizard -->
	<script type="text/javascript" src="<?php echo adminroot; ?>plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
	

	<script>
	$(document).ready(function(){
		"use strict";
		FormComponents.init(); // Init all form-specific plugins
	});
	</script>
	<?php }?>
	<?php if(in_array('chart',$js)) {?>
	<!-- Charts -->
	<script type="text/javascript" src="<?php echo adminroot; ?>plugins/sparkline/jquery.sparkline.min.js"></script>

	<script type="text/javascript" src="<?php echo adminroot; ?>plugins/daterangepicker/moment.min.js"></script>
	<script type="text/javascript" src="<?php echo adminroot; ?>plugins/daterangepicker/daterangepicker.js"></script>
	<script type="text/javascript" src="<?php echo adminroot; ?>plugins/blockui/jquery.blockUI.min.js"></script>

	<?php }?>
	<?php if(in_array('DataTables',$js)) {?>
	<!-- DataTables -->
	<script type="text/javascript" src="<?php echo adminroot; ?>plugins/datatables/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="<?php echo adminroot; ?>plugins/datatables/DT_bootstrap.js"></script>
	<script type="text/javascript" src="<?php echo adminroot; ?>plugins/datatables/responsive/datatables.responsive.js"></script> <!-- optional -->
	<?php }?>
	
	<!-- Alerts -->
	<script type="text/javascript" src="<?php echo adminroot; ?>plugins/alerts/jquery.alerts.js"></script>
	<link href="<?php echo adminroot; ?>plugins/alerts/jquery.alerts.css" rel="stylesheet" type="text/css" />
	<?php if($headcode) echo $headcode;?>

</head>

<body>
	<?php if(!$clear) {?>
	<!-- Header -->
	<header class="header navbar navbar-fixed-top" role="banner">
		<!-- Top Navigation Bar -->
		<div class="container">
			<!-- Only visible on smartphones, menu toggle -->
			<ul class="nav navbar-nav">
				<li class="nav-toggle"><a href="javascript:void(0);" title=""><i class="icon-reorder"></i></a></li>
			</ul>

			<!-- Logo -->
			<a class="navbar-brand" href="<?php echo adminroot; ?>"><strong>I Talk I See</strong></a>
			<!-- /logo -->

			<!-- Sidebar Toggler -->
			<a href="#" class="toggle-sidebar bs-tooltip" data-placement="bottom">
				<i class="icon-reorder"></i>
			</a>
			<!-- /Sidebar Toggler -->

			<!-- Top Left Menu not hidden-sm -->
			<ul class="nav navbar-nav navbar-left">
                <?php  echo '<li><a class="icon-copy" href="'.adminroot.'app"> Recording</a></li>'; ?>
			</ul>
			<!-- /Top Left Menu -->

			<!-- Top Right Menu -->
			<ul class="nav navbar-nav navbar-right">
				<!-- User Login Dropdown -->
				<li class="dropdown user">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<i class="icon-user"></i>
						<span class="username">Admin</span>
						<i class="icon-caret-down small"></i>
					</a>
					<ul class="dropdown-menu">
						<li><a href="<?php echo adminroot; ?>general/mypass.php"><i class="icon-unlock-alt"></i>Edit Pass</a></li>
						<li><a href="<?php echo adminroot; ?>?out=1"><i class="icon-key"></i>Log out</a></li>
					</ul>
				</li>
				<!-- /user login dropdown -->
			</ul>
			<!-- /Top Right Menu -->
		</div>
		<!-- /top navigation bar -->
	</header> <!-- /.header -->

	<div id="container">
		<div id="sidebar" class="sidebar-fixed">
			<div id="sidebar-content">

				<!--=== Navigation ===-->
				<ul id="nav">
					<?php if (function_exists('submodulemenus')) {
					 submodulemenus();
					}?>
				</ul>
				<!-- /Navigation -->
			</div>
			<div id="divider" class="resizeable"></div>
		</div>
		<!-- /Sidebar -->

		<div id="content">
			<div class="container">
	<?php }?>	
<?php
}
function htmlfoot($clear=0)
{
?>
	<?php if(!$clear) {?>
			</div>
			<!-- /.container -->
		</div>
	</div>
	<?php }?>	
</body>
</html>
<?php
}
?>
